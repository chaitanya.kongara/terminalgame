from board import *
from variables import *
from input import *
from ball import *
from bricks import *
from end import *
from falling import *
import os
import time
import sys
os.system('clear')

print('██████╗░██████╗░██╗░█████╗░██╗░░██╗  ██████╗░██████╗░███████╗░█████╗░██╗░░██╗███████╗██████╗░'.center(200))
print('██╔══██╗██╔══██╗██║██╔══██╗██║░██╔╝  ██╔══██╗██╔══██╗██╔════╝██╔══██╗██║░██╔╝██╔════╝██╔══██╗'.center(200))
print('██████╦╝██████╔╝██║██║░░╚═╝█████═╝░  ██████╦╝██████╔╝█████╗░░███████║█████═╝░█████╗░░██████╔╝'.center(200))
print('██╔══██╗██╔══██╗██║██║░░██╗██╔═██╗░  ██╔══██╗██╔══██╗██╔══╝░░██╔══██║██╔═██╗░██╔══╝░░██╔══██╗'.center(200))
print('██████╦╝██║░░██║██║╚█████╔╝██║░╚██╗  ██████╦╝██║░░██║███████╗██║░░██║██║░╚██╗███████╗██║░░██║'.center(200))
print('╚═════╝░╚═╝░░╚═╝╚═╝░╚════╝░╚═╝░░╚═╝  ╚═════╝░╚═╝░░╚═╝╚══════╝╚═╝░░╚═╝╚═╝░░╚═╝╚══════╝╚═╝░░╚═╝'.center(200))
os.system('aplay -q ./sounds/initial.wav')


Score = 0
os.system('clear')
print("\033[0;0H")
screen = board(ROWS,COLUMNS)
inp = KBHit()
ball = Ball(screen.start,screen.paddle_length)
ball.x=screen.move_paddle_ball(screen.start,ball.x,ball.y)
screen.add_bricks(bricks)
screen.print_board(LIVES,Score,0)
ball_left_paddle=False
time_start=0
letter = 'p'
done = 0
falling = False
fireball = False
while True and LIVES!=0 and done < 32:
    if (TIME+time.time()-time_start) > 10:
        falling = True
    letter = inp.getinput()
    if letter == 'q':
        break
    if letter == 'f':
        if fireball == False:
            fireball = True
        elif fireball == True:
            fireball = False
    if ball_left_paddle==False and letter == 'a':
        ball.x=screen.move_paddle_ball(screen.start-(screen.paddle_length//2),ball.x,ball.y)
        print("\033[0;0H")
        screen.print_board(LIVES,Score,TIME)
    elif ball_left_paddle==False and letter == 'd':
        ball.x=screen.move_paddle_ball(screen.start+(screen.paddle_length//2),ball.x,ball.y)
        print("\033[0;0H")
        screen.print_board(LIVES,Score,TIME)
    elif ball_left_paddle==False and letter == ' ':
        ball_left_paddle=True
        time_start=time.time()
        time_store=time_start
    if ball_left_paddle==True and letter == 'a':
        screen.move_paddle(screen.start-(screen.paddle_length//2))
        print("\033[0;0H")
        screen.print_board(LIVES,Score,TIME+time.time()-time_start)
    elif ball_left_paddle==True and letter == 'd':
        screen.move_paddle(screen.start+(screen.paddle_length//2))
        print("\033[0;0H")
        screen.print_board(LIVES,Score,TIME+time.time()-time_start)
    print("\033[0;0H")
    if ball_left_paddle==False:
        screen.print_board(LIVES,Score,TIME)
    else:
        screen.print_board(LIVES,Score,TIME+time.time()-time_start)
    if ball_left_paddle==True and (time.time()-time_store) > 0.2:
        time_store=time.time()
        if ball.x+0.2*ball.vx<COLUMNS-1 and ball.x+0.2*ball.vx>=1 and ball.y+0.2*ball.vy>=1 and ball.y+0.2*ball.vy<ROWS-2:
            flag=False
            for i in range(33):
                if int(ball.x+0.2*ball.vx)>=bricks[i].x-1 and int(ball.x+0.2*ball.vx)<=bricks[i].x+6 and int(ball.y+0.2*ball.vy)>=bricks[i].y-1 and int(ball.y+0.2*ball.vy)<=bricks[i].y+3 and bricks[i].strength!=0:
                    for k in range(1,abs(int(0.2*ball.vx))+2):
                        if k == abs(int(0.2*ball.vx))+1 and int(0.2*ball.vx) == 0.2*ball.vx:
                            pass
                        elif k == abs(int(0.2*ball.vx))+1 and int(0.2*ball.vx) != 0.2*ball.vx:
                            k=abs(0.2*ball.vx)
                        k=k*(abs(int(0.2*ball.vx))/int(0.2*ball.vx))
                        if int(ball.x+k)>=bricks[i].x-1 and int(ball.x+k)<=bricks[i].x+6 and int(ball.y+0.2*ball.vy)>=bricks[i].y-1 and int(ball.y+0.2*ball.vy)<=bricks[i].y+3:
                            if int(ball.x+k) == bricks[i].x-1 and int(ball.y+0.2*ball.vy) == bricks[i].y-1:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                Score += 1
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')                                             
                                if abs(ball.x-k)<bricks[i].x-1 and abs(ball.y-0.2*ball.vy)<bricks[i].y-1:
                                    ball.collision_wall('l|')
                                    ball.collision_wall('t-')
                                elif abs(ball.x-k)<bricks[i].x-1 and abs(ball.y-0.2*ball.vy)>bricks[i].y-1:
                                    ball.collision_wall('l|')
                                elif abs(ball.x-k)>bricks[i].x-1 and abs(ball.y-0.2*ball.vy)<bricks[i].y-1:
                                    ball.collision_wall('t-')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.x+k) == bricks[i].x+6 and int(ball.y+0.2*ball.vy) == bricks[i].y-1:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                Score += 1
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                if abs(ball.x-k)<bricks[i].x+6 and abs(ball.y-0.2*ball.vy)<bricks[i].y-1:
                                    ball.collision_wall('t-')
                                elif abs(ball.x-k)>bricks[i].x+6 and abs(ball.y-0.2*ball.vy)>bricks[i].y-1:
                                    ball.collision_wall('l|')
                                elif abs(ball.x-k)>bricks[i].x+6 and abs(ball.y-0.2*ball.vy)<bricks[i].y-1:
                                    ball.collision_wall('t-')
                                    ball.collision_wall('l|')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.x+k) == bricks[i].x-1 and int(ball.y+0.2*ball.vy) == bricks[i].y+3:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                Score += 1
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                if abs(ball.x-k)<bricks[i].x-1 and abs(ball.y-0.2*ball.vy)<bricks[i].y+3:
                                    ball.collision_wall('l|')
                                elif abs(ball.x-k)<bricks[i].x-1 and abs(ball.y-0.2*ball.vy)>bricks[i].y+3:
                                    ball.collision_wall('l|')
                                    ball.collision_wall('t-')
                                elif abs(ball.x-k)>bricks[i].x-1 and abs(ball.y-0.2*ball.vy)>bricks[i].y+3:
                                    ball.collision_wall('t-')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.x+k) == bricks[i].x+6 and int(ball.y+0.2*ball.vy) == bricks[i].y+3:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                Score += 1
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                if abs(ball.x-k)<bricks[i].x+6 and abs(ball.y-0.2*ball.vy)>bricks[i].y+3:
                                    ball.collision_wall('t-')
                                elif abs(ball.x-k)>bricks[i].x+6 and abs(ball.y-0.2*ball.vy)>bricks[i].y+3:
                                    ball.collision_wall('l|')
                                    ball.collision_wall('t-')
                                elif abs(ball.x-k)>bricks[i].x+6 and abs(ball.y-0.2*ball.vy)<bricks[i].y+3:
                                    ball.collision_wall('l|')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.x+k) == bricks[i].x-1:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                Score += 1
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                ball.collision_wall('l|')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.x+k) == bricks[i].x+6:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                Score += 1
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done +=  1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                ball.collision_wall('l|')
                                print("\033[0;0H")
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                flag=True
                                break
                            elif int(ball.y+0.2*ball.vy) == bricks[i].y-1:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &') 
                                Score += 1 
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if l == i:
                                                done += 1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')                          
                                ball.collision_wall('t-')
                                print("\033[0;0H")
                                flag=True
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                break
                            elif int(ball.y+0.2*ball.vy) == bricks[i].y+3:
                                screen.move_ball(ball.x,ball.y,k,0.2*ball.vy)
                                ball.x+=k
                                ball.y+=0.2*ball.vy
                                bricks[i].strength-=1
                                os.system('aplay -q ./sounds/brick.wav &')
                                if i>30:
                                    bricks[i].rainbow_hit=True
                                Score += 1
                                if bricks[i].strength == 0:
                                    done+=1
                                    screen.strength_zero(i)
                                if fireball:
                                    for l in range(33):
                                        if (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 3) or (abs(bricks[i].x-bricks[l].x) == 6 and  abs(bricks[i].y-bricks[l].y) == 0) or (abs(bricks[i].x-bricks[l].x) == 0 and  abs(bricks[i].y-bricks[l].y) == 0):
                                            if bricks[l].strength != 0 and bricks[l].strength <= 3:
                                                done += 1
                                            bricks[l].strength = 0
                                            screen.strength_zero(l)
                                    os.system('aplay -q ./sounds/Explosion.wav &')    
                                ball.collision_wall('t-')
                                print("\033[0;0H")
                                flag=True
                                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                                break
                    if flag == True:
                        break
            if flag==False:
                screen.move_ball(ball.x,ball.y,0.2*ball.vx,0.2*ball.vy)
                ball.x+=0.2*ball.vx
                ball.y+=0.2*ball.vy
                print("\033[0;0H")
                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
        elif ball.x+0.2*ball.vx<COLUMNS-1 and ball.x+0.2*ball.vx>=1 and int(ball.y+0.2*ball.vy) == ROWS-2:
            if int(ball.x+0.2*ball.vx)>=screen.start and  int(ball.x+0.2*ball.vx)<screen.paddle_length+screen.start and (int(ball.x+0.2*ball.vx)>=(COLUMNS-1) or int(ball.x+0.2*ball.vx)==1):
                screen.move_ball(ball.x,ball.y,0.2*ball.vx,0)
                ball.x+=0.2*ball.vx
                ball.vx+=(((ball.x-screen.start)//4)-2)
                if falling == True:
                    os.system('aplay -q ./sounds/fall.wav &')
                    screen,bricks,var = activate_falling(screen,bricks,Score,TIME+time.time()-time_start)
                    if var:
                        LIVES = 0
                        print("\033[0;0H")
                        screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                        time.sleep(1)
                        break
                if abs(ball.vx)<5:
                    ball.vx=5*(abs(ball.vx)/ball.vx)
                if abs(ball.vx)>20:
                    ball.vx=20*(abs(ball.vx)/ball.vx)
                ball.vx=(-1)*ball.vx
                ball.vy=(-1)*ball.vy
                print("\033[0;0H")
                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
            elif int(ball.x+0.2*ball.vx)>=screen.start and  int(ball.x+0.2*ball.vx)<screen.paddle_length+screen.start:
                screen.move_ball(ball.x,ball.y,0.2*ball.vx,0.2*ball.vy)
                ball.x+=0.2*ball.vx
                ball.y+=0.2*ball.vy
                ball.vx+=(((ball.x-screen.start)//4)-2)
                if falling == True:
                    os.system('aplay -q ./sounds/fall.wav &')
                    screen,bricks,var = activate_falling(screen,bricks,Score,TIME+time.time()-time_start)
                    if var:
                        LIVES = 0
                        print("\033[0;0H")
                        screen.print_board(LIVES,Score,TIME+time.time()-time_start)
                        time.sleep(1)
                        break
                os.system('aplay -q ./sounds/pad.wav &')
                if abs(ball.vx)<5:
                    ball.vx=5*(abs(ball.vx)/ball.vx)
                if abs(ball.vx)>20:
                    ball.vx=20*(abs(ball.vx)/ball.vx)
                ball.vy=(-1)*ball.vy
                print("\033[0;0H")
                screen.print_board(LIVES,Score,TIME+time.time()-time_start)
            else:
                screen.move_ball(ball.x,ball.y,0.2*ball.vx,0.2*ball.vy)
                ball.x+=0.2*ball.vx
                ball.y+=0.2*ball.vy
                print("\033[0;0H")
                screen.print_board(LIVES,Score,TIME+time.time()-time_start)        
        else:
            # for k in range(1,abs(int(0.2*ball.vx))+2):

            if int(ball.x+0.2*ball.vx) >= COLUMNS-1 and int(ball.y+0.2*ball.vy)<=0:
                ball.collision_wall('r|')
                ball.collision_wall('t-')
                os.system('aplay -q ./sounds/wall.wav &')
            elif int(ball.x+0.2*ball.vx) <= 0 and int(ball.y+0.2*ball.vy) <=0:
                ball.collision_wall('l|')
                ball.collision_wall('t-')
                os.system('aplay -q ./sounds/wall.wav &')
            elif int(ball.x+0.2*ball.vx) >= COLUMNS-1:
                ball.collision_wall('r|')
                os.system('aplay -q ./sounds/wall.wav &')
            elif int(ball.x+0.2*ball.vx)<=0:
                ball.collision_wall('l|')
                os.system('aplay -q ./sounds/wall.wav &')
            elif int(ball.y+0.2*ball.vy)<=0:
                ball.collision_wall('t-')
                os.system('aplay -q ./sounds/wall.wav &')
            else:
                LIVES= LIVES-1
                os.system('aplay -q ./sounds/lose_life.wav &')
                ball.x=screen.ball_missed_paddle(ball.x,ball.y)
                ball_left_paddle = False
                ball.vy=-5
                if ((ball.x-screen.start)//5)-2 < 0:
                    ball.vx=-10
                else:
                    ball.vx=10
                TIME += time.time()-time_start
                ball.y=ROWS-2
                print("\033[0;0H")
                screen.print_board(LIVES,Score,TIME)
game=end()
if letter == 'q':
    game.quit(Score,TIME+time.time()-time_start)
elif done == 32:
    game.you_won(Score,TIME+time.time()-time_start)
else:
    game.game_over(Score,TIME+time.time()-time_start)