import numpy as np
import random
from variables import *
from bricks import *
import colorama
import os
import textwrap
from colorama import Fore, Back, Style
colorama.init(autoreset=True)
class board:
    def __init__(self,rows,columns):
        self.row=rows
        self.col=columns
        self.paddle_length=(columns-2)//5
        self.start=int((columns-2)/2-self.paddle_length/2)+1
        self.board = np.full((rows,columns),' ')
        self.board[rows-2,self.start:self.start+self.paddle_length]='_'
        self.board[0,:]='-'
        self.board[rows-1,:]='-'
        self.board[1:rows-1,0]='|'
        self.board[1:rows-1,columns-1]='|'
    def print_board(self,Lives,Score,Time):
        print(f' {Fore.CYAN}{Back.BLUE}{Style.BRIGHT}LIVES Left:{Lives}                 Score:{Score}                  Time:{int(Time)}           q-quit                         ')
        flag2=False
        flag1=False
        for i in range(self.row):
            print(' ',end="")
            for j in range(self.col):
                if i==self.row-2 and self.board[i,j]=='_':
                    print(f"{Fore.MAGENTA}_",end="")
                else:
                    flag=False
                    for k in range(33):
                        if j>=bricks[k].x and j<=bricks[k].x+5 and i>=bricks[k].y and i<=bricks[k].y+2 and bricks[k].strength != 0:
                            flag=True
                            if k > 30 and bricks[k].rainbow_hit == False:
                                bricks[k].strength=random.randint(1,3)
                                bricks[k].rainbow_hit = True
                                if k == 31:
                                    flag1 = True
                                else:
                                    flag2=True
                            if bricks[k].strength == 3:
                                print(Fore.BLACK+Back.YELLOW+self.board[i,j],end="")
                            elif bricks[k].strength == 2:
                                print(Fore.BLACK+Back.BLUE+self.board[i,j],end="")
                            elif bricks[k].strength == 1:
                                print(Fore.BLACK+Back.GREEN+self.board[i,j],end="")
                            elif bricks[k].strength > 3:
                                print(Fore.BLACK+Back.RED+self.board[i,j],end="")
                            break
                    if flag == False:
                        print(self.board[i,j],end="")
            print('')
        if flag2 == True:
            bricks[32].rainbow_hit = False
        if flag1 == True:
            bricks[31].rainbow_hit = False
    def move_paddle_ball(self,start,ball_x,ball_y):
        rows=ROWS
        columns=COLUMNS
        if (start >= self.start and start <= columns-2-self.paddle_length+1) or (start<=self.start and start > 0):
            ball_x=ball_x-self.start+start
            self.start=start
            self.board[rows-2,1:columns-1]=' '
            self.board[rows-2,start:start+self.paddle_length]='_'
            self.board[rows-2,int(ball_x)]='O'
        return ball_x
    def move_ball(self,ball_x,ball_y,ball_vx,ball_vy):
        self.board[int(ball_y),int(ball_x)]=' '
        self.board[ROWS-2,self.start:self.start+self.paddle_length]='_'
        self.board[int(ball_y+ball_vy),int(ball_x+ball_vx)]='O'
    def move_paddle(self,start):
        rows=ROWS
        columns=COLUMNS
        if (start >= self.start and start <= columns-2-self.paddle_length+1) or (start<=self.start and start > 0):
            self.start=start
            self.board[rows-2,1:columns-1]=' '
            self.board[rows-2,start:start+self.paddle_length]='_'
    def ball_missed_paddle(self,ball_x,ball_y):
        rows=ROWS
        if(int(ball_y)!=rows-1 and int(ball_x)>0 and int(ball_x)<COLUMNS-1 ):
            self.board[int(ball_y),int(ball_x)]=' '
        ball_x=self.start+random.randint(1,self.paddle_length-2)
        self.board[rows-2,self.start:self.start+self.paddle_length]='_'
        self.board[rows-2,ball_x]='O'
        return ball_x
    def add_bricks(self,bricks):
        for i in range(33):
            if True:
                self.board[bricks[i].y:bricks[i].y+3,bricks[i].x:bricks[i].x+6] = '-'
                self.board[bricks[i].y+1,bricks[i].x:bricks[i].x+6]='|'
                self.board[bricks[i].y+1,bricks[i].x+1:bricks[i].x+5]=' '
    def strength_zero(self,i):
        self.board[bricks[i].y:bricks[i].y+3,bricks[i].x:bricks[i].x+6] = ' '