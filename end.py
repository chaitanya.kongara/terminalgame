import textwrap
import os
class end:
    def game_over(self,Score,TIME):
        TIME=int(TIME)
        os.system('clear')
        print("\t\t\t _______  _______  __   __  _______    _______  __   __  _______  ______\n" +  
              "\t\t\t|       ||   _   ||  |_|  ||       |  |       ||  | |  ||       ||    _ |\n"  +
              "\t\t\t|    ___||  |_|  ||       ||    ___|  |   _   ||  |_|  ||    ___||   | ||\n"  +
              "\t\t\t|   | __ |       ||       ||   |___   |  | |  ||       ||   |___ |   |_||_      "+ f'SCORE = {Score}\n' +
              "\t\t\t|   ||  ||       ||       ||    ___|  |  |_|  ||       ||    ___||    __  |\n"+
              "\t\t\t|   |_| ||   _   || ||_|| ||   |___   |       | |     | |   |___ |   |  | |     "+ f'Time Played = {TIME}\n'+
              "\t\t\t|_______||__| |__||_|   |_||_______|  |_______|  |___|  |_______||___|  |_|\n")
        os.system('aplay -q ./sounds/gameover.wav &')
    def you_won(self,Score,TIME):
        os.system('clear')
        TIME=int(TIME)
        print( "\t\t\t __   __  ___   _______  _______  _______  ______    __   __\n" +   
               "\t\t\t|  | |  ||   | |       ||       ||       ||    _ |  |  | |  |\n" +  
               "\t\t\t|  |_|  ||   | |      _||_     _||   _   ||   | ||  |  |_|  |\n" +  
               "\t\t\t|       ||   | |     |    |   |  |  | |  ||   |_||_ |       |        " + f'Time Taken = {TIME}\n'  
               "\t\t\t|       ||   | |     |    |   |  |  |_|  ||    __  ||_     _|\n" +  
               "\t\t\t |     | |   | |     |_   |   |  |       ||   |  | |  |   |\n" +    
               "\t\t\t  |___|  |___| |_______|  |___|  |_______||___|  |_|  |___|\n" )
        os.system('aplay -q ./sounds/success.wav &')
    def quit(self,Score,TIME):
        os.system('clear')
        TIME=int(TIME)
        print("\t\t\t __   __  _______  __   __      _______  __   __  ___   _______ \n" +
              "\t\t\t|  | |  ||       ||  | |  |    |       ||  | |  ||   | |       |\n" +
              "\t\t\t|  |_|  ||   _   ||  | |  |    |   _   ||  | |  ||   | |_     _|      "+ f'SCORE = {Score}\n' +
              "\t\t\t|       ||  | |  ||  |_|  |    |  | |  ||  |_|  ||   |   |   |  \n" +
              "\t\t\t|_     _||  |_|  ||       |    |  |_|  ||       ||   |   |   |       "+ f'Time Played = {TIME}\n'+
              "\t\t\t  |   |  |       ||       |    |      | |       ||   |   |   |  \n" +
              "\t\t\t  |___|  |_______||_______|    |____||_||_______||___|   |___|  \n" )