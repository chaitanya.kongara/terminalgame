from brick import *
import numpy as np
x_coor=20
y_coor=5
bricks=np.empty(40,dtype=object)
for i in range(1,16):
    bricks[i-1]=brick_breakable(x_coor,y_coor)
    x_coor += 6
    if i%3 == 0:
        y_coor += 3
        x_coor=20
x_coor=50
y_coor=5
for i in range(1,16):
    bricks[14+i]=brick_breakable(x_coor,y_coor)
    x_coor += 6
    if i%3 == 0:
        y_coor += 3
        x_coor=50
bricks[30]=brick_unbreakable(41,10)
bricks[32]=brick_rainbow(41,5)
bricks[31]=brick_rainbow(41,20)