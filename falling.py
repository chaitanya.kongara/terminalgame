from variables import *
def activate_falling(screen,bricks,score,time):
    i = ROWS-2
    flag = False
    while i>1:
        screen.board[i,:]=screen.board[i-1,:]
        i-=1
    for i in range(1,COLUMNS):
        screen.board[1,i]=' '
    screen.board[1,0]='|'
    screen.board[1,COLUMNS-1]='|'
    for i in range(33):
        bricks[i].y+=1
        if bricks[i].y+2 >= ROWS-2 and bricks[i].strength!=0: 
            flag=True
    return screen, bricks, flag