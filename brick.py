import numpy as np
import random
class brick_parent:
    def __init__(self,x,y):
        self.x=x
        self.y=y
class brick_breakable(brick_parent):
    def __init__(self,x,y):
        super().__init__(x,y)
        self.strength = random.randint(1,3)
class brick_unbreakable(brick_parent):
    def __init__(self,x,y):
        super().__init__(x,y)
        self.strength=100000
class brick_rainbow(brick_parent):
    def __init__(self,x,y):
        super().__init__(x,y)
        self.strength=random.randint(1,3)
        self.rainbow_hit = False
