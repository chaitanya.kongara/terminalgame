import random
from variables import *
class Ball:
    def __init__(self,start,paddle_length):
        self.x=start+random.randint(1,paddle_length-2)
        self.y= ROWS-2
        if ((self.x-start)//5)-2 >= 0:
            self.vx=10
        else:
            self.vx=-10
        self.vy=-5
    def collision_wall(self,ch):
        if ch == 'l|' or ch == 'r|':
            self.vx=(-1)*self.vx
        elif ch == 't-':
            self.vy=(-1)*self.vy