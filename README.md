# Brick Breaker

Terminal based game similar to Brick breaker game.
# FULFILL THE REQUIREMENTS

Using the following command `pip3 install -r requirements.txt`  

# Command to start the game

python3 main.py

# Controls 

* 'a' to move the paddle left
* 'd' to move the paddle right
* '` `'  to activate the movement of the ball
* 'q' to quit the game
* 'f' to activate the fireball powerup

# Rules of the game

* Manage to break all bricks without losing all 3 lives
* Losing all 3 lives result in a Game Over
* Each hit to the bricks weakens it by 1 unit
* Red brick indicates unbreakable brick
* Yellow brick indicates brick with 3 units of strength
* Blue brick indicates brick with 2 units of strength
* Green brick indicates brick with 1 units of strength
* Each hit to the brick results in gain of 1 point, which gets added to the score
* Destroying a brick using fireball powerup results in gain of 100 points
* After 10 seconds from the start of the game the bricks starts falling down, every time the ball hits the paddle the brick layout falls down by 1 unit
* There is another special kind of bricks namely rainbow bricks which keep on changes color until they get a hit, after getting a hit with the ball, the strength of the brick  becomes one less than the strength present just before the collision.   

# OOPS:

* __Inheritance__:

    Used while creating different types of bricks.

* __Polymorphism__:
    1. Over-riding:

        Defining methods in the child class with same name asthe methods defined in parent class
    2. Over-loading:
        
        Making a method behave differntly when different types / number of aruguments passed to the methods
* __Encapsulation__:

    Classes, Objects and methods exist for all the objects

* __Abstraction__:

    Allmost used everywhere eg: print_board() to print the board.